resource "google_cloud_run_service" "demo" {
  name                       = local.service_name
  location                   = var.region
  autogenerate_revision_name = true

  template {
    spec {
      containers {
        image = data.external.image_digest.result.image
      }
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location = google_cloud_run_service.demo.location
  project  = google_cloud_run_service.demo.project
  service  = google_cloud_run_service.demo.name

  policy_data = data.google_iam_policy.noauth.policy_data
  depends_on  = [google_cloud_run_service.demo]
}

data "external" "image_digest" {
  program = ["bash", "scripts/get_latest_tag.sh", var.projectId, local.service_name]
}
