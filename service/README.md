# Base service

Build and run base service 🚀

```bash
 $ docker build -t cloudrun-demo .
 $ docker run -p 8080:8080 -d --name cloudrun-demo cloudrun-demo
```

Visit [http://localhost:8080](http://localhost:8080) to see result