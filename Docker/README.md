# Create custom gcloud sdk image

## Requirements

- [Docker hub account](https://hub.docker.com/)

```bash
$ docker login -u $DOCKER_HUB_USERNAME

$ docker build -t $DOCKER_HUB_USERNAME/gcloud-sdk . 

$ docker push $DOCKER_HUB_USERNAME/gcloud-sdk  
```