# Cloud Run Demo

Build and deploy base service

## Requirements 

- [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- GCP project (Replace bellow $GCP_ID for project id )
- Service account key file in project root with owner permission (Replace bellow $GCP fot path to file)

```sh
$ export GOOGLE_APPLICATION_CREDENTIALS=$GCP

$ export GOOGLE_PROJECT_ID=$GCP_ID

$ gcloud auth activate-service-account --key-file=$GOOGLE_APPLICATION_CREDENTIALS

$ gcloud builds submit --project $GOOGLE_PROJECT_ID

$ terraform init

$ terraform apply --var projectId=$GOOGLE_PROJECT_ID
```