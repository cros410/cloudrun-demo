variable "projectId" {
  type        = string
  description = "GCP Project Id"
}

variable "region" {
  default = "us-central1"
  type    = string
}