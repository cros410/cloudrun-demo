#!/bin/bash

PROJECT=$1
SERVICE_NAME=$2

LATEST=$(gcloud container images describe gcr.io/${PROJECT}/${SERVICE_NAME}:latest  --format="value(image_summary.fully_qualified_digest)" | tr -d '\n')
echo "{\"image\": \"${LATEST}\"}"