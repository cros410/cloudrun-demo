terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.53"
    }
  }
}

provider "google" {
  project = var.projectId
}

locals {
  service_folder = "service"
  service_name   = "cloudrun-demo"
}